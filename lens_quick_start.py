import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('proj_name')
parser.add_argument('version', nargs='?', default='v1.1')
args = parser.parse_args()


proj_name = args.proj_name
version = args.version

subprocess.run([f'raft.py init-project -p {proj_name}'], shell=True)
subprocess.run([f'ls projects/{proj_name}'], shell=True)
subprocess.run([f'mkdir -p projects/{proj_name}/workflow/bin'], shell=True)
subprocess.run([f'wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/329db9653ec2d64d9f23eb04de835325/clean_work_files.sh -P projects/{proj_name}/workflow/bin'], shell=True)
subprocess.run([f'chmod +x projects/{proj_name}/workflow/bin/clean_work_files.sh'], shell=True)

print('Loading human references')
subprocess.run([f'raft.py load-reference -p {proj_name} -f cta_and_self_antigen.homo_sapiens.gene_list'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f Homo_sapiens.assembly38.no_ebv.fa'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f dummy_file'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f Hsap38.txt'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f gencode.v37.annotation.gff3'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f gencode.v37.annotation.with.hervs.gtf'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f GRCh38.GENCODEv37'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f af-only-gnomad.hg38.vcf.gz'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f 1000g_pon.hg38.vcf.gz'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f small_exac_common_3.hg38.vcf.gz'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f Homo_sapiens_assembly38.dbsnp138.vcf.gz'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f peptidome.homo_sapiens'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f gencode.v37.pc_translations.fa'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f hg38_exome.bed'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f tcga_transcript_tpm_summary.tsv'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f virdetect.cds.gff'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f virus_masked_hg38.fa'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f virus.cds.fa'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f virus.pep.fa'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f antigen.garnish'], shell=True)
subprocess.run([f'raft.py load-reference -p {proj_name} -f mhcflurry'], shell=True)

print('Load LENS module')
subprocess.run([f'raft.py load-module -p {proj_name} -m lens -b {version}'], shell=True)

print('Add parse_immuno_manifest workflow')
subprocess.run([f'raft.py add-step -p {proj_name} -m immuno -s parse_immuno_manifest'], shell=True)

print('Add manifest_to_lens workflow')
subprocess.run([f'raft.py add-step -p {proj_name} -m lens -s manifest_to_lens'], shell=True)

print('Add workflow parameters')
subprocess.run([f'raft.py copy-parameters -c projects/{proj_name}/workflow/lens/lens.default_params.config -d {proj_name}'], shell=True)
subprocess.run([f'cp projects/{proj_name}/workflow/main.nf.copy_params projects/{proj_name}/workflow/main.nf'], shell=True)
