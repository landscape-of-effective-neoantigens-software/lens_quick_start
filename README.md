# LENS Quick Start

A Python script for creating a default, human LENS project.

Execute by `cd`ing to your RAFT installation and running:

```
python lens_quick_start.py <YOUR_PROJ_NAME>
```

This script performs the following functionality:
- Creates the project
- Loads the appropriate references
- Loads the LENS module and its dependencies
- Adds `extract_manifest_from_channel` and `manifest_to_lens` to the project's `main.nf`
- Copies default parameters into the project's `main.nf`

Users should provide their:
- Paired end FASTQs
- Manifest (mapping FASTQs to samples to patients)
- Docker images for NetMHC* tools (see tutorial)
